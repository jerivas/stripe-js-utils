# Stripe.js utils

Client-side utilities for payment processing with stripe.js.

## Installation

Install from git via npm. The package will be saved as `stripe-js-utils`.

```bash
# Latest version
npm install --save git+https://gitlab.com/jerivas/stripe-js-utils.git

# Specific version (add tag at the end)
npm install --save git+https://gitlab.com/jerivas/stripe-js-utils.git#v1.1.0
```

## Usage

The module is UMD compatible, so you can `require` or `import` it in your code. For example, using ES6 modules:

```javascript
import { StripeForm, elementsProvider } from 'stripe-js-utils';
```

You can also use the bundle directly from the `dist` folder, which will create a `stripeJsUtils` object available globally.

```html
<script src="dist/stripe-js-utils.min.js"></script>
```

## Browser support

`StripeForm.getSource()` requires Promise support.

## What's included

### Stripe Providers

The providers allow you to use Stripe's client-side libraries without having to manually include a `<script>` in your HTML. They also check if the corresponding Stripe object has been already attached to the `window` to avoid loading it again.

```javascript
function onElementsReady(Stripe) {
    console.log("The Elements object is ready, take a look:", Stripe);
    // Put all the code that requires the Stripe object here...
}

function onCheckoutsReady(StripeCheckout) {
    console.log("The Checkout object is ready, take a look:", StripeCheckout);
    // Put all the code that requires the StripeCheckout object here...
}

elementsProvider(onElementsReady);
checkoutProvider(onCheckoutReady);
```

Learn more about the [Stripe object] and [StripeCheckout object].

### Stripe Form

The `StripeForm` class will mount [Stripe Elements] in your HTML form. It will also display live error messages and can provide you with a [Source object] when the user has entered all their card info. With the Source object you can use Stripe's API on the server to collect a payment.

Note that `StripeForm` uses `elementsProvider` in the background, so you can use `StripeForm` directly.

Your form markup should include a few DOM nodes to mount the Elements and display error messages. Optionally you can add a hidden input to store the Source ID when the form is submitted.

```html
<form id="payment-form" method="POST" action="">
    <!-- The Source ID will be added here -->
    <input type="hidden" name="stripe_source">

    <!-- Elements required markup -->
    <div class="card-wrapper">
        <label>Credit or debit card</label>
        <div class="card-element"></div>
        <div class="card-errors" role="alert"></div>
    </div>

    <input type="submit">
</form>
```

```javascript
const form = document.querySelector('#payment-form');
const stripeForm = new StripeForm({
    form: form,
    key: '<public Stripe key here>',
    elementOptions: {}, // See https://stripe.com/docs/stripe.js#element-options
});

// Create the Source object and save it's ID to a field on submit
form.addEventListener('submit', event => {
    event.preventDefault();
    stripeForm.getSource(source => {
        form.elements['stripe_source'].value = source.id;
        form.submit();
    });
});
```

[Stripe object]: https://stripe.com/docs/stripe.js#the-stripe-object
[StripeCheckout object]: https://stripe.com/docs/checkout#integration-custom
[Stripe Elements]: https://stripe.com/docs/elements
[Source object]: https://stripe.com/docs/api#sources
