/**
 * Insert a script and execute a callback when ready.
 * Basic caching is supported.
 *
 * @param {String} url The src of the script
 * @param {String} objectName The global variable name that indicates the script has loaded
 * @param {any} readyCallback Executed when the script is loaded
 */
export function scriptProvider(url, objectName, readyCallback) {
	// Return early if we've already done all the hard work
	if (window[objectName]) {
		return readyCallback(window[objectName]);
	}

	// Prepare the script to be inserted
	const firstScript = document.getElementsByTagName('script')[0];
	const script = document.createElement('script');
	script.src = url;
	script.async = true;
	script.defer = true;

	// Execute the callback when the script loads
	script.onload = () => {
		// Delete the handler, just for memory reasons
		script.onload = undefined;
		readyCallback(window[objectName]);
	};

	// Inject the script into the DOM, which sets off the loading/execution
	firstScript.parentElement.insertBefore(script, firstScript);

	return true;
}

/**
 * Load Stripe's Elements library and execute a callback.
 *
 * @param {Function} readyCallback Will be executed when window.Stripe is ready.
 */
export function elementsProvider(readyCallback) {
	return scriptProvider(
		'https://js.stripe.com/v3/',
		'Stripe',
		readyCallback // eslint-disable-line
	);
}

/**
 * Load Stripe's Checkout library and execute a callback.
 *
 * @param {Function} readyCallback Will be executed when window.StripeCheckout is ready.
 */
export function checkoutProvider(readyCallback) {
	return scriptProvider(
		'https://checkout.stripe.com/checkout.js',
		'StripeCheckout',
		readyCallback // eslint-disable-line
	);
}
