(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["stripeJsUtils"] = factory();
	else
		root["stripeJsUtils"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["c"] = scriptProvider;
/* harmony export (immutable) */ __webpack_exports__["a"] = elementsProvider;
/* harmony export (immutable) */ __webpack_exports__["b"] = checkoutProvider;
/**
 * Insert a script and execute a callback when ready.
 * Basic caching is supported.
 *
 * @param {String} url The src of the script
 * @param {String} objectName The global variable name that indicates the script has loaded
 * @param {any} readyCallback Executed when the script is loaded
 */
function scriptProvider(url, objectName, readyCallback) {
	// Return early if we've already done all the hard work
	if (window[objectName]) {
		return readyCallback(window[objectName]);
	}

	// Prepare the script to be inserted
	var firstScript = document.getElementsByTagName('script')[0];
	var script = document.createElement('script');
	script.src = url;
	script.async = true;
	script.defer = true;

	// Execute the callback when the script loads
	script.onload = function () {
		// Delete the handler, just for memory reasons
		script.onload = undefined;
		readyCallback(window[objectName]);
	};

	// Inject the script into the DOM, which sets off the loading/execution
	firstScript.parentElement.insertBefore(script, firstScript);

	return true;
}

/**
 * Load Stripe's Elements library and execute a callback.
 *
 * @param {Function} readyCallback Will be executed when window.Stripe is ready.
 */
function elementsProvider(readyCallback) {
	return scriptProvider('https://js.stripe.com/v3/', 'Stripe', readyCallback // eslint-disable-line
	);
}

/**
 * Load Stripe's Checkout library and execute a callback.
 *
 * @param {Function} readyCallback Will be executed when window.StripeCheckout is ready.
 */
function checkoutProvider(readyCallback) {
	return scriptProvider('https://checkout.stripe.com/checkout.js', 'StripeCheckout', readyCallback // eslint-disable-line
	);
}

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_object_path_get__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_object_path_get___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_object_path_get__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stripe_provider__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var StripeForm = function () {
	/**
  * Loads Stripe.js and asynchronously calls init() when ready.
  *
  * @param {Object} { form, key, elementOptions }
  * @memberof StripeForm
  * @see init
  */
	function StripeForm(_ref) {
		var _this = this;

		var form = _ref.form,
		    key = _ref.key,
		    _ref$elementOptions = _ref.elementOptions,
		    elementOptions = _ref$elementOptions === undefined ? {} : _ref$elementOptions;

		_classCallCheck(this, StripeForm);

		if (!form) throw new Error('StripeForm requires a form element');
		if (!key) throw new Error('StripeForm requires a valid Stripe key');
		__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__stripe_provider__["a" /* elementsProvider */])(function (Stripe) {
			return _this.init(Stripe, form, key, elementOptions);
		});
	}

	/**
  * Initialize the class and set event handlers.
  *
  * @param {Object} Stripe The object provided by Stripe.js
  * @param {HTMLElement} form The form that will receive the Stripe Card Element
  * @param {String} key Public Stripe key
  * @param {Object} elementOptions Will be passed directly to Stripe Elements
  * @memberof StripeForm
  */


	_createClass(StripeForm, [{
		key: 'init',
		value: function init(Stripe, form, key, elementOptions) {
			var _this2 = this;

			var wrapper = form.querySelector('.card-wrapper');
			var cardElement = wrapper.querySelector('.card-element');
			var cardErrors = wrapper.querySelector('.card-errors');

			var stripe = Stripe(key);
			var elements = stripe.elements();
			var card = elements.create('card', elementOptions);
			card.mount(cardElement);

			// Display error messages automatically
			card.addEventListener('change', function (_ref2) {
				var error = _ref2.error;

				if (error) {
					_this2.showError(error.message);
				} else {
					_this2.clearErrors();
				}
			});

			// Set class properties
			this.form = form;
			this.stripe = stripe;
			this.card = card;
			this.wrapper = wrapper;
			this.cardErrors = cardErrors;
		}

		/**
   * Display card errors in the form.
   *
   * @param {String} message Error to be displayed
   * @memberof StripeForm
   */

	}, {
		key: 'showError',
		value: function showError(message) {
			var wrapper = this.wrapper,
			    cardErrors = this.cardErrors;

			wrapper.classList.add('has-error');
			cardErrors.textContent = message;
		}

		/**
   * Clear card errors in the form.
   *
   * @memberof StripeForm
   */

	}, {
		key: 'clearErrors',
		value: function clearErrors() {
			var wrapper = this.wrapper,
			    cardErrors = this.cardErrors;

			wrapper.classList.remove('has-error');
			cardErrors.textContent = '';
		}

		/**
   * Create a Stripe Owner object based on form fields.
   *
   * @param {HTMLElement} A form with the inputs required by the Owner
   * @memberof StripeForm
   */

	}, {
		key: 'getOwnerInfo',
		value: function getOwnerInfo() {
			var form = this.form;

			return {
				owner: {
					name: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.name.value'),
					address: {
						line1: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.line1.value'),
						line2: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.line2.value'),
						city: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.city.value'),
						state: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.state.value'),
						postal_code: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.postal_code.value'),
						country: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.country.value')
					},
					email: __WEBPACK_IMPORTED_MODULE_0_object_path_get___default()(form, 'elements.email.value')
				}
			};
		}

		/**
   * Create a Stripe Source object with the form's card and owner info.
   *
   * @param {Function} successCallback Will receive the resulting Source object
   * @memberof StripeForm
   */

	}, {
		key: 'getSource',
		value: function getSource(successCallback) {
			var _this3 = this;

			var stripe = this.stripe,
			    card = this.card;

			stripe.createSource(card, this.getOwnerInfo()).then(function (result) {
				if (result.error) {
					_this3.showError(result.error.message);
				} else {
					_this3.clearErrors();
					successCallback(result.source);
				}
			});
		}
	}]);

	return StripeForm;
}();

/* harmony default export */ __webpack_exports__["a"] = (StripeForm);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__stripe_provider__ = __webpack_require__(0);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "elementsProvider", function() { return __WEBPACK_IMPORTED_MODULE_0__stripe_provider__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "checkoutProvider", function() { return __WEBPACK_IMPORTED_MODULE_0__stripe_provider__["b"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "scriptProvider", function() { return __WEBPACK_IMPORTED_MODULE_0__stripe_provider__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stripe_form__ = __webpack_require__(1);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "StripeForm", function() { return __WEBPACK_IMPORTED_MODULE_1__stripe_form__["a"]; });



/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = exports = function (obj, path, defaultValue, delimiter) {
	if (typeof path === 'string') {
		path = path.split(delimiter || '.');
	}
	if (Array.isArray(path)) {
		var len = path.length;
		for (var i = 0; i < len; i++) {
			if (obj && (obj.hasOwnProperty(path[i]) || obj[path[i]])) {
				obj = obj[path[i]];
			} else {
				return defaultValue;
			}
		}
		return obj;
	} else {
		return defaultValue;
	}
};


/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCBhZDdlOWIxODAyMDA5MGNhYWNjYyIsIndlYnBhY2s6Ly8vLi9zcmMvc3RyaXBlLXByb3ZpZGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9zdHJpcGUtZm9ybS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vfi9vYmplY3QtcGF0aC1nZXQvaW5kZXguanMiXSwibmFtZXMiOlsic2NyaXB0UHJvdmlkZXIiLCJ1cmwiLCJvYmplY3ROYW1lIiwicmVhZHlDYWxsYmFjayIsIndpbmRvdyIsImZpcnN0U2NyaXB0IiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsInNjcmlwdCIsImNyZWF0ZUVsZW1lbnQiLCJzcmMiLCJhc3luYyIsImRlZmVyIiwib25sb2FkIiwidW5kZWZpbmVkIiwicGFyZW50RWxlbWVudCIsImluc2VydEJlZm9yZSIsImVsZW1lbnRzUHJvdmlkZXIiLCJjaGVja291dFByb3ZpZGVyIiwiU3RyaXBlRm9ybSIsImZvcm0iLCJrZXkiLCJlbGVtZW50T3B0aW9ucyIsIkVycm9yIiwiaW5pdCIsIlN0cmlwZSIsIndyYXBwZXIiLCJxdWVyeVNlbGVjdG9yIiwiY2FyZEVsZW1lbnQiLCJjYXJkRXJyb3JzIiwic3RyaXBlIiwiZWxlbWVudHMiLCJjYXJkIiwiY3JlYXRlIiwibW91bnQiLCJhZGRFdmVudExpc3RlbmVyIiwiZXJyb3IiLCJzaG93RXJyb3IiLCJtZXNzYWdlIiwiY2xlYXJFcnJvcnMiLCJjbGFzc0xpc3QiLCJhZGQiLCJ0ZXh0Q29udGVudCIsInJlbW92ZSIsIm93bmVyIiwibmFtZSIsImdldCIsImFkZHJlc3MiLCJsaW5lMSIsImxpbmUyIiwiY2l0eSIsInN0YXRlIiwicG9zdGFsX2NvZGUiLCJjb3VudHJ5IiwiZW1haWwiLCJzdWNjZXNzQ2FsbGJhY2siLCJjcmVhdGVTb3VyY2UiLCJnZXRPd25lckluZm8iLCJ0aGVuIiwicmVzdWx0Iiwic291cmNlIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztBQ1ZBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1EQUEyQyxjQUFjOztBQUV6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7QUNoRUE7QUFBQTs7Ozs7Ozs7QUFRTyxTQUFTQSxjQUFULENBQXdCQyxHQUF4QixFQUE2QkMsVUFBN0IsRUFBeUNDLGFBQXpDLEVBQXdEO0FBQzlEO0FBQ0EsS0FBSUMsT0FBT0YsVUFBUCxDQUFKLEVBQXdCO0FBQ3ZCLFNBQU9DLGNBQWNDLE9BQU9GLFVBQVAsQ0FBZCxDQUFQO0FBQ0E7O0FBRUQ7QUFDQSxLQUFNRyxjQUFjQyxTQUFTQyxvQkFBVCxDQUE4QixRQUE5QixFQUF3QyxDQUF4QyxDQUFwQjtBQUNBLEtBQU1DLFNBQVNGLFNBQVNHLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZjtBQUNBRCxRQUFPRSxHQUFQLEdBQWFULEdBQWI7QUFDQU8sUUFBT0csS0FBUCxHQUFlLElBQWY7QUFDQUgsUUFBT0ksS0FBUCxHQUFlLElBQWY7O0FBRUE7QUFDQUosUUFBT0ssTUFBUCxHQUFnQixZQUFNO0FBQ3JCO0FBQ0FMLFNBQU9LLE1BQVAsR0FBZ0JDLFNBQWhCO0FBQ0FYLGdCQUFjQyxPQUFPRixVQUFQLENBQWQ7QUFDQSxFQUpEOztBQU1BO0FBQ0FHLGFBQVlVLGFBQVosQ0FBMEJDLFlBQTFCLENBQXVDUixNQUF2QyxFQUErQ0gsV0FBL0M7O0FBRUEsUUFBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7O0FBS08sU0FBU1ksZ0JBQVQsQ0FBMEJkLGFBQTFCLEVBQXlDO0FBQy9DLFFBQU9ILGVBQ04sMkJBRE0sRUFFTixRQUZNLEVBR05HLGFBSE0sQ0FHUTtBQUhSLEVBQVA7QUFLQTs7QUFFRDs7Ozs7QUFLTyxTQUFTZSxnQkFBVCxDQUEwQmYsYUFBMUIsRUFBeUM7QUFDL0MsUUFBT0gsZUFDTix5Q0FETSxFQUVOLGdCQUZNLEVBR05HLGFBSE0sQ0FHUTtBQUhSLEVBQVA7QUFLQSxDOzs7Ozs7Ozs7Ozs7OztBQzFERDs7QUFFQTs7SUFFTWdCLFU7QUFDTDs7Ozs7OztBQU9BLDJCQUFnRDtBQUFBOztBQUFBLE1BQWxDQyxJQUFrQyxRQUFsQ0EsSUFBa0M7QUFBQSxNQUE1QkMsR0FBNEIsUUFBNUJBLEdBQTRCO0FBQUEsaUNBQXZCQyxjQUF1QjtBQUFBLE1BQXZCQSxjQUF1Qix1Q0FBTixFQUFNOztBQUFBOztBQUMvQyxNQUFJLENBQUNGLElBQUwsRUFBVyxNQUFNLElBQUlHLEtBQUosQ0FBVSxvQ0FBVixDQUFOO0FBQ1gsTUFBSSxDQUFDRixHQUFMLEVBQVUsTUFBTSxJQUFJRSxLQUFKLENBQVUsd0NBQVYsQ0FBTjtBQUNWTixFQUFBLGlHQUFBQSxDQUFpQjtBQUFBLFVBQVUsTUFBS08sSUFBTCxDQUFVQyxNQUFWLEVBQWtCTCxJQUFsQixFQUF3QkMsR0FBeEIsRUFBNkJDLGNBQTdCLENBQVY7QUFBQSxHQUFqQjtBQUNBOztBQUVEOzs7Ozs7Ozs7Ozs7O3VCQVNLRyxNLEVBQVFMLEksRUFBTUMsRyxFQUFLQyxjLEVBQWdCO0FBQUE7O0FBQ3ZDLE9BQU1JLFVBQVVOLEtBQUtPLGFBQUwsQ0FBbUIsZUFBbkIsQ0FBaEI7QUFDQSxPQUFNQyxjQUFjRixRQUFRQyxhQUFSLENBQXNCLGVBQXRCLENBQXBCO0FBQ0EsT0FBTUUsYUFBYUgsUUFBUUMsYUFBUixDQUFzQixjQUF0QixDQUFuQjs7QUFFQSxPQUFNRyxTQUFTTCxPQUFPSixHQUFQLENBQWY7QUFDQSxPQUFNVSxXQUFXRCxPQUFPQyxRQUFQLEVBQWpCO0FBQ0EsT0FBTUMsT0FBT0QsU0FBU0UsTUFBVCxDQUFnQixNQUFoQixFQUF3QlgsY0FBeEIsQ0FBYjtBQUNBVSxRQUFLRSxLQUFMLENBQVdOLFdBQVg7O0FBRUE7QUFDQUksUUFBS0csZ0JBQUwsQ0FBc0IsUUFBdEIsRUFBZ0MsaUJBQWU7QUFBQSxRQUFaQyxLQUFZLFNBQVpBLEtBQVk7O0FBQzlDLFFBQUlBLEtBQUosRUFBVztBQUNWLFlBQUtDLFNBQUwsQ0FBZUQsTUFBTUUsT0FBckI7QUFDQSxLQUZELE1BRU87QUFDTixZQUFLQyxXQUFMO0FBQ0E7QUFDRCxJQU5EOztBQVFBO0FBQ0EsUUFBS25CLElBQUwsR0FBWUEsSUFBWjtBQUNBLFFBQUtVLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFFBQUtFLElBQUwsR0FBWUEsSUFBWjtBQUNBLFFBQUtOLE9BQUwsR0FBZUEsT0FBZjtBQUNBLFFBQUtHLFVBQUwsR0FBa0JBLFVBQWxCO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozs0QkFNVVMsTyxFQUFTO0FBQUEsT0FDVlosT0FEVSxHQUNjLElBRGQsQ0FDVkEsT0FEVTtBQUFBLE9BQ0RHLFVBREMsR0FDYyxJQURkLENBQ0RBLFVBREM7O0FBRWxCSCxXQUFRYyxTQUFSLENBQWtCQyxHQUFsQixDQUFzQixXQUF0QjtBQUNBWixjQUFXYSxXQUFYLEdBQXlCSixPQUF6QjtBQUNBOztBQUVEOzs7Ozs7OztnQ0FLYztBQUFBLE9BQ0xaLE9BREssR0FDbUIsSUFEbkIsQ0FDTEEsT0FESztBQUFBLE9BQ0lHLFVBREosR0FDbUIsSUFEbkIsQ0FDSUEsVUFESjs7QUFFYkgsV0FBUWMsU0FBUixDQUFrQkcsTUFBbEIsQ0FBeUIsV0FBekI7QUFDQWQsY0FBV2EsV0FBWCxHQUF5QixFQUF6QjtBQUNBOztBQUVEOzs7Ozs7Ozs7aUNBTWU7QUFBQSxPQUNOdEIsSUFETSxHQUNHLElBREgsQ0FDTkEsSUFETTs7QUFFZCxVQUFPO0FBQ053QixXQUFPO0FBQ05DLFdBQU0sdURBQUFDLENBQUkxQixJQUFKLEVBQVUscUJBQVYsQ0FEQTtBQUVOMkIsY0FBUztBQUNSQyxhQUFPLHVEQUFBRixDQUFJMUIsSUFBSixFQUFVLHNCQUFWLENBREM7QUFFUjZCLGFBQU8sdURBQUFILENBQUkxQixJQUFKLEVBQVUsc0JBQVYsQ0FGQztBQUdSOEIsWUFBTSx1REFBQUosQ0FBSTFCLElBQUosRUFBVSxxQkFBVixDQUhFO0FBSVIrQixhQUFPLHVEQUFBTCxDQUFJMUIsSUFBSixFQUFVLHNCQUFWLENBSkM7QUFLUmdDLG1CQUFhLHVEQUFBTixDQUFJMUIsSUFBSixFQUFVLDRCQUFWLENBTEw7QUFNUmlDLGVBQVMsdURBQUFQLENBQUkxQixJQUFKLEVBQVUsd0JBQVY7QUFORCxNQUZIO0FBVU5rQyxZQUFPLHVEQUFBUixDQUFJMUIsSUFBSixFQUFVLHNCQUFWO0FBVkQ7QUFERCxJQUFQO0FBY0E7O0FBRUQ7Ozs7Ozs7Ozs0QkFNVW1DLGUsRUFBaUI7QUFBQTs7QUFBQSxPQUNsQnpCLE1BRGtCLEdBQ0QsSUFEQyxDQUNsQkEsTUFEa0I7QUFBQSxPQUNWRSxJQURVLEdBQ0QsSUFEQyxDQUNWQSxJQURVOztBQUUxQkYsVUFBTzBCLFlBQVAsQ0FBb0J4QixJQUFwQixFQUEwQixLQUFLeUIsWUFBTCxFQUExQixFQUErQ0MsSUFBL0MsQ0FBb0Qsa0JBQVU7QUFDN0QsUUFBSUMsT0FBT3ZCLEtBQVgsRUFBa0I7QUFDakIsWUFBS0MsU0FBTCxDQUFlc0IsT0FBT3ZCLEtBQVAsQ0FBYUUsT0FBNUI7QUFDQSxLQUZELE1BRU87QUFDTixZQUFLQyxXQUFMO0FBQ0FnQixxQkFBZ0JJLE9BQU9DLE1BQXZCO0FBQ0E7QUFDRCxJQVBEO0FBUUE7Ozs7OztBQUdGLHlEQUFlekMsVUFBZixFOzs7Ozs7Ozs7Ozs7OztBQ3hIQTs7Ozs7Ozs7QUNBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsU0FBUztBQUMxQjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSIsImZpbGUiOiJzdHJpcGUtanMtdXRpbHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJzdHJpcGVKc1V0aWxzXCJdID0gZmFjdG9yeSgpO1xuXHRlbHNlXG5cdFx0cm9vdFtcInN0cmlwZUpzVXRpbHNcIl0gPSBmYWN0b3J5KCk7XG59KSh0aGlzLCBmdW5jdGlvbigpIHtcbnJldHVybiBcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG5cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgYWQ3ZTliMTgwMjAwOTBjYWFjY2MiLCIvKipcbiAqIEluc2VydCBhIHNjcmlwdCBhbmQgZXhlY3V0ZSBhIGNhbGxiYWNrIHdoZW4gcmVhZHkuXG4gKiBCYXNpYyBjYWNoaW5nIGlzIHN1cHBvcnRlZC5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gdXJsIFRoZSBzcmMgb2YgdGhlIHNjcmlwdFxuICogQHBhcmFtIHtTdHJpbmd9IG9iamVjdE5hbWUgVGhlIGdsb2JhbCB2YXJpYWJsZSBuYW1lIHRoYXQgaW5kaWNhdGVzIHRoZSBzY3JpcHQgaGFzIGxvYWRlZFxuICogQHBhcmFtIHthbnl9IHJlYWR5Q2FsbGJhY2sgRXhlY3V0ZWQgd2hlbiB0aGUgc2NyaXB0IGlzIGxvYWRlZFxuICovXG5leHBvcnQgZnVuY3Rpb24gc2NyaXB0UHJvdmlkZXIodXJsLCBvYmplY3ROYW1lLCByZWFkeUNhbGxiYWNrKSB7XG5cdC8vIFJldHVybiBlYXJseSBpZiB3ZSd2ZSBhbHJlYWR5IGRvbmUgYWxsIHRoZSBoYXJkIHdvcmtcblx0aWYgKHdpbmRvd1tvYmplY3ROYW1lXSkge1xuXHRcdHJldHVybiByZWFkeUNhbGxiYWNrKHdpbmRvd1tvYmplY3ROYW1lXSk7XG5cdH1cblxuXHQvLyBQcmVwYXJlIHRoZSBzY3JpcHQgdG8gYmUgaW5zZXJ0ZWRcblx0Y29uc3QgZmlyc3RTY3JpcHQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF07XG5cdGNvbnN0IHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuXHRzY3JpcHQuc3JjID0gdXJsO1xuXHRzY3JpcHQuYXN5bmMgPSB0cnVlO1xuXHRzY3JpcHQuZGVmZXIgPSB0cnVlO1xuXG5cdC8vIEV4ZWN1dGUgdGhlIGNhbGxiYWNrIHdoZW4gdGhlIHNjcmlwdCBsb2Fkc1xuXHRzY3JpcHQub25sb2FkID0gKCkgPT4ge1xuXHRcdC8vIERlbGV0ZSB0aGUgaGFuZGxlciwganVzdCBmb3IgbWVtb3J5IHJlYXNvbnNcblx0XHRzY3JpcHQub25sb2FkID0gdW5kZWZpbmVkO1xuXHRcdHJlYWR5Q2FsbGJhY2sod2luZG93W29iamVjdE5hbWVdKTtcblx0fTtcblxuXHQvLyBJbmplY3QgdGhlIHNjcmlwdCBpbnRvIHRoZSBET00sIHdoaWNoIHNldHMgb2ZmIHRoZSBsb2FkaW5nL2V4ZWN1dGlvblxuXHRmaXJzdFNjcmlwdC5wYXJlbnRFbGVtZW50Lmluc2VydEJlZm9yZShzY3JpcHQsIGZpcnN0U2NyaXB0KTtcblxuXHRyZXR1cm4gdHJ1ZTtcbn1cblxuLyoqXG4gKiBMb2FkIFN0cmlwZSdzIEVsZW1lbnRzIGxpYnJhcnkgYW5kIGV4ZWN1dGUgYSBjYWxsYmFjay5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZWFkeUNhbGxiYWNrIFdpbGwgYmUgZXhlY3V0ZWQgd2hlbiB3aW5kb3cuU3RyaXBlIGlzIHJlYWR5LlxuICovXG5leHBvcnQgZnVuY3Rpb24gZWxlbWVudHNQcm92aWRlcihyZWFkeUNhbGxiYWNrKSB7XG5cdHJldHVybiBzY3JpcHRQcm92aWRlcihcblx0XHQnaHR0cHM6Ly9qcy5zdHJpcGUuY29tL3YzLycsXG5cdFx0J1N0cmlwZScsXG5cdFx0cmVhZHlDYWxsYmFjayAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG5cdCk7XG59XG5cbi8qKlxuICogTG9hZCBTdHJpcGUncyBDaGVja291dCBsaWJyYXJ5IGFuZCBleGVjdXRlIGEgY2FsbGJhY2suXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcmVhZHlDYWxsYmFjayBXaWxsIGJlIGV4ZWN1dGVkIHdoZW4gd2luZG93LlN0cmlwZUNoZWNrb3V0IGlzIHJlYWR5LlxuICovXG5leHBvcnQgZnVuY3Rpb24gY2hlY2tvdXRQcm92aWRlcihyZWFkeUNhbGxiYWNrKSB7XG5cdHJldHVybiBzY3JpcHRQcm92aWRlcihcblx0XHQnaHR0cHM6Ly9jaGVja291dC5zdHJpcGUuY29tL2NoZWNrb3V0LmpzJyxcblx0XHQnU3RyaXBlQ2hlY2tvdXQnLFxuXHRcdHJlYWR5Q2FsbGJhY2sgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuXHQpO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3N0cmlwZS1wcm92aWRlci5qcyIsImltcG9ydCBnZXQgZnJvbSAnb2JqZWN0LXBhdGgtZ2V0JztcblxuaW1wb3J0IHsgZWxlbWVudHNQcm92aWRlciB9IGZyb20gJy4vc3RyaXBlLXByb3ZpZGVyJztcblxuY2xhc3MgU3RyaXBlRm9ybSB7XG5cdC8qKlxuXHQgKiBMb2FkcyBTdHJpcGUuanMgYW5kIGFzeW5jaHJvbm91c2x5IGNhbGxzIGluaXQoKSB3aGVuIHJlYWR5LlxuXHQgKlxuXHQgKiBAcGFyYW0ge09iamVjdH0geyBmb3JtLCBrZXksIGVsZW1lbnRPcHRpb25zIH1cblx0ICogQG1lbWJlcm9mIFN0cmlwZUZvcm1cblx0ICogQHNlZSBpbml0XG5cdCAqL1xuXHRjb25zdHJ1Y3Rvcih7IGZvcm0sIGtleSwgZWxlbWVudE9wdGlvbnMgPSB7fSB9KSB7XG5cdFx0aWYgKCFmb3JtKSB0aHJvdyBuZXcgRXJyb3IoJ1N0cmlwZUZvcm0gcmVxdWlyZXMgYSBmb3JtIGVsZW1lbnQnKTtcblx0XHRpZiAoIWtleSkgdGhyb3cgbmV3IEVycm9yKCdTdHJpcGVGb3JtIHJlcXVpcmVzIGEgdmFsaWQgU3RyaXBlIGtleScpO1xuXHRcdGVsZW1lbnRzUHJvdmlkZXIoU3RyaXBlID0+IHRoaXMuaW5pdChTdHJpcGUsIGZvcm0sIGtleSwgZWxlbWVudE9wdGlvbnMpKTtcblx0fVxuXG5cdC8qKlxuXHQgKiBJbml0aWFsaXplIHRoZSBjbGFzcyBhbmQgc2V0IGV2ZW50IGhhbmRsZXJzLlxuXHQgKlxuXHQgKiBAcGFyYW0ge09iamVjdH0gU3RyaXBlIFRoZSBvYmplY3QgcHJvdmlkZWQgYnkgU3RyaXBlLmpzXG5cdCAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGZvcm0gVGhlIGZvcm0gdGhhdCB3aWxsIHJlY2VpdmUgdGhlIFN0cmlwZSBDYXJkIEVsZW1lbnRcblx0ICogQHBhcmFtIHtTdHJpbmd9IGtleSBQdWJsaWMgU3RyaXBlIGtleVxuXHQgKiBAcGFyYW0ge09iamVjdH0gZWxlbWVudE9wdGlvbnMgV2lsbCBiZSBwYXNzZWQgZGlyZWN0bHkgdG8gU3RyaXBlIEVsZW1lbnRzXG5cdCAqIEBtZW1iZXJvZiBTdHJpcGVGb3JtXG5cdCAqL1xuXHRpbml0KFN0cmlwZSwgZm9ybSwga2V5LCBlbGVtZW50T3B0aW9ucykge1xuXHRcdGNvbnN0IHdyYXBwZXIgPSBmb3JtLnF1ZXJ5U2VsZWN0b3IoJy5jYXJkLXdyYXBwZXInKTtcblx0XHRjb25zdCBjYXJkRWxlbWVudCA9IHdyYXBwZXIucXVlcnlTZWxlY3RvcignLmNhcmQtZWxlbWVudCcpO1xuXHRcdGNvbnN0IGNhcmRFcnJvcnMgPSB3cmFwcGVyLnF1ZXJ5U2VsZWN0b3IoJy5jYXJkLWVycm9ycycpO1xuXG5cdFx0Y29uc3Qgc3RyaXBlID0gU3RyaXBlKGtleSk7XG5cdFx0Y29uc3QgZWxlbWVudHMgPSBzdHJpcGUuZWxlbWVudHMoKTtcblx0XHRjb25zdCBjYXJkID0gZWxlbWVudHMuY3JlYXRlKCdjYXJkJywgZWxlbWVudE9wdGlvbnMpO1xuXHRcdGNhcmQubW91bnQoY2FyZEVsZW1lbnQpO1xuXG5cdFx0Ly8gRGlzcGxheSBlcnJvciBtZXNzYWdlcyBhdXRvbWF0aWNhbGx5XG5cdFx0Y2FyZC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoeyBlcnJvciB9KSA9PiB7XG5cdFx0XHRpZiAoZXJyb3IpIHtcblx0XHRcdFx0dGhpcy5zaG93RXJyb3IoZXJyb3IubWVzc2FnZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHR0aGlzLmNsZWFyRXJyb3JzKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHQvLyBTZXQgY2xhc3MgcHJvcGVydGllc1xuXHRcdHRoaXMuZm9ybSA9IGZvcm07XG5cdFx0dGhpcy5zdHJpcGUgPSBzdHJpcGU7XG5cdFx0dGhpcy5jYXJkID0gY2FyZDtcblx0XHR0aGlzLndyYXBwZXIgPSB3cmFwcGVyO1xuXHRcdHRoaXMuY2FyZEVycm9ycyA9IGNhcmRFcnJvcnM7XG5cdH1cblxuXHQvKipcblx0ICogRGlzcGxheSBjYXJkIGVycm9ycyBpbiB0aGUgZm9ybS5cblx0ICpcblx0ICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgRXJyb3IgdG8gYmUgZGlzcGxheWVkXG5cdCAqIEBtZW1iZXJvZiBTdHJpcGVGb3JtXG5cdCAqL1xuXHRzaG93RXJyb3IobWVzc2FnZSkge1xuXHRcdGNvbnN0IHsgd3JhcHBlciwgY2FyZEVycm9ycyB9ID0gdGhpcztcblx0XHR3cmFwcGVyLmNsYXNzTGlzdC5hZGQoJ2hhcy1lcnJvcicpO1xuXHRcdGNhcmRFcnJvcnMudGV4dENvbnRlbnQgPSBtZXNzYWdlO1xuXHR9XG5cblx0LyoqXG5cdCAqIENsZWFyIGNhcmQgZXJyb3JzIGluIHRoZSBmb3JtLlxuXHQgKlxuXHQgKiBAbWVtYmVyb2YgU3RyaXBlRm9ybVxuXHQgKi9cblx0Y2xlYXJFcnJvcnMoKSB7XG5cdFx0Y29uc3QgeyB3cmFwcGVyLCBjYXJkRXJyb3JzIH0gPSB0aGlzO1xuXHRcdHdyYXBwZXIuY2xhc3NMaXN0LnJlbW92ZSgnaGFzLWVycm9yJyk7XG5cdFx0Y2FyZEVycm9ycy50ZXh0Q29udGVudCA9ICcnO1xuXHR9XG5cblx0LyoqXG5cdCAqIENyZWF0ZSBhIFN0cmlwZSBPd25lciBvYmplY3QgYmFzZWQgb24gZm9ybSBmaWVsZHMuXG5cdCAqXG5cdCAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IEEgZm9ybSB3aXRoIHRoZSBpbnB1dHMgcmVxdWlyZWQgYnkgdGhlIE93bmVyXG5cdCAqIEBtZW1iZXJvZiBTdHJpcGVGb3JtXG5cdCAqL1xuXHRnZXRPd25lckluZm8oKSB7XG5cdFx0Y29uc3QgeyBmb3JtIH0gPSB0aGlzO1xuXHRcdHJldHVybiB7XG5cdFx0XHRvd25lcjoge1xuXHRcdFx0XHRuYW1lOiBnZXQoZm9ybSwgJ2VsZW1lbnRzLm5hbWUudmFsdWUnKSxcblx0XHRcdFx0YWRkcmVzczoge1xuXHRcdFx0XHRcdGxpbmUxOiBnZXQoZm9ybSwgJ2VsZW1lbnRzLmxpbmUxLnZhbHVlJyksXG5cdFx0XHRcdFx0bGluZTI6IGdldChmb3JtLCAnZWxlbWVudHMubGluZTIudmFsdWUnKSxcblx0XHRcdFx0XHRjaXR5OiBnZXQoZm9ybSwgJ2VsZW1lbnRzLmNpdHkudmFsdWUnKSxcblx0XHRcdFx0XHRzdGF0ZTogZ2V0KGZvcm0sICdlbGVtZW50cy5zdGF0ZS52YWx1ZScpLFxuXHRcdFx0XHRcdHBvc3RhbF9jb2RlOiBnZXQoZm9ybSwgJ2VsZW1lbnRzLnBvc3RhbF9jb2RlLnZhbHVlJyksXG5cdFx0XHRcdFx0Y291bnRyeTogZ2V0KGZvcm0sICdlbGVtZW50cy5jb3VudHJ5LnZhbHVlJyksXG5cdFx0XHRcdH0sXG5cdFx0XHRcdGVtYWlsOiBnZXQoZm9ybSwgJ2VsZW1lbnRzLmVtYWlsLnZhbHVlJyksXG5cdFx0XHR9LFxuXHRcdH07XG5cdH1cblxuXHQvKipcblx0ICogQ3JlYXRlIGEgU3RyaXBlIFNvdXJjZSBvYmplY3Qgd2l0aCB0aGUgZm9ybSdzIGNhcmQgYW5kIG93bmVyIGluZm8uXG5cdCAqXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IHN1Y2Nlc3NDYWxsYmFjayBXaWxsIHJlY2VpdmUgdGhlIHJlc3VsdGluZyBTb3VyY2Ugb2JqZWN0XG5cdCAqIEBtZW1iZXJvZiBTdHJpcGVGb3JtXG5cdCAqL1xuXHRnZXRTb3VyY2Uoc3VjY2Vzc0NhbGxiYWNrKSB7XG5cdFx0Y29uc3QgeyBzdHJpcGUsIGNhcmQgfSA9IHRoaXM7XG5cdFx0c3RyaXBlLmNyZWF0ZVNvdXJjZShjYXJkLCB0aGlzLmdldE93bmVySW5mbygpKS50aGVuKHJlc3VsdCA9PiB7XG5cdFx0XHRpZiAocmVzdWx0LmVycm9yKSB7XG5cdFx0XHRcdHRoaXMuc2hvd0Vycm9yKHJlc3VsdC5lcnJvci5tZXNzYWdlKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRoaXMuY2xlYXJFcnJvcnMoKTtcblx0XHRcdFx0c3VjY2Vzc0NhbGxiYWNrKHJlc3VsdC5zb3VyY2UpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFN0cmlwZUZvcm07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvc3RyaXBlLWZvcm0uanMiLCJleHBvcnQgeyBlbGVtZW50c1Byb3ZpZGVyLCBjaGVja291dFByb3ZpZGVyLCBzY3JpcHRQcm92aWRlciB9IGZyb20gJy4vc3RyaXBlLXByb3ZpZGVyJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgU3RyaXBlRm9ybSB9IGZyb20gJy4vc3RyaXBlLWZvcm0nO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2luZGV4LmpzIiwiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMgPSBmdW5jdGlvbiAob2JqLCBwYXRoLCBkZWZhdWx0VmFsdWUsIGRlbGltaXRlcikge1xuXHRpZiAodHlwZW9mIHBhdGggPT09ICdzdHJpbmcnKSB7XG5cdFx0cGF0aCA9IHBhdGguc3BsaXQoZGVsaW1pdGVyIHx8ICcuJyk7XG5cdH1cblx0aWYgKEFycmF5LmlzQXJyYXkocGF0aCkpIHtcblx0XHR2YXIgbGVuID0gcGF0aC5sZW5ndGg7XG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuXHRcdFx0aWYgKG9iaiAmJiAob2JqLmhhc093blByb3BlcnR5KHBhdGhbaV0pIHx8IG9ialtwYXRoW2ldXSkpIHtcblx0XHRcdFx0b2JqID0gb2JqW3BhdGhbaV1dO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIGRlZmF1bHRWYWx1ZTtcblx0XHRcdH1cblx0XHR9XG5cdFx0cmV0dXJuIG9iajtcblx0fSBlbHNlIHtcblx0XHRyZXR1cm4gZGVmYXVsdFZhbHVlO1xuXHR9XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L29iamVjdC1wYXRoLWdldC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwic291cmNlUm9vdCI6IiJ9