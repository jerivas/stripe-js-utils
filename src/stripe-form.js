import get from 'object-path-get';

import { elementsProvider } from './stripe-provider';

class StripeForm {
	/**
	 * Loads Stripe.js and asynchronously calls init() when ready.
	 *
	 * @param {Object} { form, key, elementOptions }
	 * @memberof StripeForm
	 * @see init
	 */
	constructor({ form, key, elementOptions = {} }) {
		if (!form) throw new Error('StripeForm requires a form element');
		if (!key) throw new Error('StripeForm requires a valid Stripe key');
		elementsProvider(Stripe => this.init(Stripe, form, key, elementOptions));
	}

	/**
	 * Initialize the class and set event handlers.
	 *
	 * @param {Object} Stripe The object provided by Stripe.js
	 * @param {HTMLElement} form The form that will receive the Stripe Card Element
	 * @param {String} key Public Stripe key
	 * @param {Object} elementOptions Will be passed directly to Stripe Elements
	 * @memberof StripeForm
	 */
	init(Stripe, form, key, elementOptions) {
		const wrapper = form.querySelector('.card-wrapper');
		const cardElement = wrapper.querySelector('.card-element');
		const cardErrors = wrapper.querySelector('.card-errors');

		const stripe = Stripe(key);
		const elements = stripe.elements();
		const card = elements.create('card', elementOptions);
		card.mount(cardElement);

		// Display error messages automatically
		card.addEventListener('change', ({ error }) => {
			if (error) {
				this.showError(error.message);
			} else {
				this.clearErrors();
			}
		});

		// Set class properties
		this.form = form;
		this.stripe = stripe;
		this.card = card;
		this.wrapper = wrapper;
		this.cardErrors = cardErrors;
	}

	/**
	 * Display card errors in the form.
	 *
	 * @param {String} message Error to be displayed
	 * @memberof StripeForm
	 */
	showError(message) {
		const { wrapper, cardErrors } = this;
		wrapper.classList.add('has-error');
		cardErrors.textContent = message;
	}

	/**
	 * Clear card errors in the form.
	 *
	 * @memberof StripeForm
	 */
	clearErrors() {
		const { wrapper, cardErrors } = this;
		wrapper.classList.remove('has-error');
		cardErrors.textContent = '';
	}

	/**
	 * Create a Stripe Owner object based on form fields.
	 *
	 * @param {HTMLElement} A form with the inputs required by the Owner
	 * @memberof StripeForm
	 */
	getOwnerInfo() {
		const { form } = this;
		return {
			owner: {
				name: get(form, 'elements.name.value'),
				address: {
					line1: get(form, 'elements.line1.value'),
					line2: get(form, 'elements.line2.value'),
					city: get(form, 'elements.city.value'),
					state: get(form, 'elements.state.value'),
					postal_code: get(form, 'elements.postal_code.value'),
					country: get(form, 'elements.country.value'),
				},
				email: get(form, 'elements.email.value'),
			},
		};
	}

	/**
	 * Create a Stripe Source object with the form's card and owner info.
	 *
	 * @param {Function} successCallback Will receive the resulting Source object
	 * @memberof StripeForm
	 */
	getSource(successCallback) {
		const { stripe, card } = this;
		stripe.createSource(card, this.getOwnerInfo()).then(result => {
			if (result.error) {
				this.showError(result.error.message);
			} else {
				this.clearErrors();
				successCallback(result.source);
			}
		});
	}
}

export default StripeForm;
